import React from 'react';
import { Link } from 'react-router-dom';
import './subpage.css';
// import Subimages from  './Subimages';
import LiveNews from './LiveNews';
import Bestinsports from './Bestinsports';
// import MoviesList from './MoviesList';
let Subpage3 = function ({ match }) {
    return <div className="body1"> 
   <div id="header">
 <div className="topnav" id="header">
 <nav id="topnav">
<div className="dropdown" id="dropdown1">
<button className="dropbtn" id="toggle">
<div className="bar1"></div>
<div className="bar2"></div>
<div className="bar3"></div>
<i className="fa "></i>
</button>
<Link to="/">
<p className="dropbtn"id="title">hotstar<i className="fa "></i> </p>
</Link>
<div className="dropdown-content1">
<div className="dropdown-content" >
<a href="#" id="active1"><i className="fa fa-home"></i>&emsp;HOME</a>
<a href="#"><i className="fa fa-television"></i>&emsp;TV</a>
<a href="#"><i className="fa fa-life-bouy"></i>&emsp;MOVIE</a>
<a href="#" ><i className="fa fa-life-ring"></i>&emsp;SPORTS</a>
<a href="#"><i className="fa fa-newspaper-o"></i>&emsp;NEWS</a>
<a href="#"><i className="fas fa-star"></i>&emsp;PREMIUM</a>
<a href="#" ><i className="fa fa-television"></i>&emsp;CHANNELS</a>
<a href="#"><i className="fa fa-language"></i>&emsp;LANGUAGES</a>
<a href="#"><i className="fas fa-gift"></i>&emsp;GENRES</a>
</div> </div>
</div> 

<div className="dropdown" id="dropdown1">
<button className="dropbtn" >
TV
<i className="fa "></i>
</button>
<div className="dropdown-content">
<div className="dropdown-content" >
<a href="#">StarPlus</a>
<a href="#">Star jalsha</a>
<a href="#">Star Vijay</a>
<a href="#">Star Bharat</a>
<a href="#">Life Ok</a>
<a href="#">Asianet</a>
<a href="#">Star Maa</a>
<a href="#">Star World</a>
<a href="#">More...</a>
</div> </div>
</div> 
<div className="dropdown" id="dropdown1">
<button className="dropbtn" id="dropbtn1" >MOVIES
<i className="fa "></i>
</button>
<div className="dropdown-content">
<div className="dropdown-content" >
<a href="#">Hindi</a>
<a href="#">Bengali</a>
<a href="#">Telugu</a>
<a href="#">Malayalam</a>
<a href="#">Tamil</a>
<a href="#">Marathi</a>
<a href="#">English</a>
<a href="#">Kannada</a>
<a href="#">Gujarati</a>
</div> </div>
</div> 
<div className="dropdown" id="dropdown1">
<button className="dropbtn" id="dropbtn2">SPORTS
<i className="fa "></i>
</button>
<div className="dropdown-content">
<div className="dropdown-content" >
<a href="#">Kabaddi</a>
<a href="#">Football</a>
<a href="#">Badminton</a>
<a href="#">Hockey</a>
<a href="#">Tennis</a>
<a href="#">Formula 1</a>
<a href="#">Formula 2 </a>
<a href="#">Table Tennis</a>
<a href="#">Athletics</a>
<a href="#">Golf</a>
<a href="#">Swimming</a>
<a href="#">eSports</a>
<a href="#">Boxing</a>
</div> </div>
</div> 
<div className="dropdown">
<button className="dropbtn" id="dropdownspace4">NEWS
<i className="fa "></i>
</button></div>
<div className="dropdown">
<button className="dropbtn"id="dropdownspace5">PREMIUM
<i className="fa "></i>
</button></div>
<div className="dropdown">
<button className="dropbtn" id="dropdownspace6">SEARCH
<form >
<input type="search" name="search" placeholder="Search.."/>
</form>
<i className="fa "></i>
</button></div>
<div className="dropdown">
<button onClick={()=>document.getElementById('id01').style.display='block'} width="auto" className="dropbtn" id="dropdownspace7">SIGN IN </button>
<div id="id01" className="modal">
<form className="modal-content animate" id ="loginform1" action="/action_page.php">
<div className="imgcontainer">
<span onClick={() => document.getElementById('id01').style.display = 'none'} className="close" title="Close Modal">&times;</span>
<p className="signin1"  >Sign In</p>
</div>
<div className="facebooklogin"><div className="fb-wrap">
<img className="fbicon" src ="https://www.freeiconspng.com/uploads/facebook-f-logo-white-background-21.jpg" alt="img"/><span className="facebooktext" id="fbtxt">SIGN IN WITH FACEBOOK</span>
</div></div>
<div className="divhr1"><hr className="hrwidth1"/><p className="por">OR</p>
<hr className="hrwidth2"/></div>
<div className="inputtype"><input type="email" className="email" id="email1"name="Email" placeholder="Enter Email"/>
<input type="password" className="email" id="email2" name="pwd" placeholder="Enter Password" ></input>
<div className="facebooklogin1">
<span className="logintext" id="fbtxt">SIGN IN</span>
</div>
<div className="forgotpwd">Forgot Password</div>
</div>
</form>
</div>
</div>
</nav>
</div>
<div className="containerdiv1sub">
<Bestinsports id={match.params.id}/>


{/*<h2>{match.params.id}</h2>*/}
</div>


<div className="footer">
  <span className="footerspan">
  <span className="footertxt"><a className="footertxta" href="#">About Hotstar</a></span>
  <span className="footertxt"><a className="footertxta" href="#">Terms Of Use</a></span>
  <span className="footertxt"><a className="footertxta" href="#"> Privacy Policy</a></span>
  <span className="footertxt"><a className="footertxta" href="#">FAQ</a></span>
  <span className="footertxt"><a className="footertxta" href="#">Feedback</a></span>
  <span className="footertxt"><a className="footertxta" href="#">Careers</a></span>
  <span className="footertxt1"><a className="footertxta1"id="footertxtspace" href="#"><b className="b1"> Connect With us </b></a>
  </span>
  <span className="footertxt1"><a className="footertxta1" id="footertxtspace1" href="#"><b className="b1">Hotstar App</b></a>
  </span>
  <p className="footerpara">© 2018 STAR. All Rights Reserved. HBO, Home Box Office and all related channel and programming logos are service marks of, and all related programming visuals and elements are the property of, Home Box Office, Inc. All rights reserved.</p>
  </span>
 <span className="fab fa-facebook-f"id="footerfacebook" font-Size="48px">
 </span>
 <span className="fa fa-twitter"id="footertwitter" font-Size="48px"></span>
 <div className="footerplaystorebox">
 <span className="footerplaystore">
 <p className="footerplaystoretxt">get it on</p>
 <p className="footerplaystoretxt1">Google Play</p>
 <img className="footerplaystore1"  src="http://www.rabsnetsolutions.com/wp-content/uploads/2017/05/playstore.png"alt="img"/>
 <a className="playstore" href="https://play.google.com/store/apps/details?id=in.startv.hotstar" target="_blank" ></a>
 </span></div>
 <div className="footerappstorebox">
 <span className="footerappstore">
 <p className="footerappstoretxt">Download on the</p>
 <p className="footerappstoretxt1">App Store</p>
 <img className="footerappstore1"  src="http://www.p2p2p2.com/img173/mfrvpqafjfyshlltstma.png"alt="img"/>
 <a className="playstore" href="https://play.google.com/store/apps/details?id=in.startv.hotstar" target="_blank" data-reactid="274"></a>
 </span></div>
 
  </div>


   
   

   </div></div>
}

        
      
export default Subpage3;
