import React from 'react';
import {Link} from 'react-router-dom';
class MoviessList extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
        MoviessListImgs:[{
          id: 0,
          video: 'https://www.youtube.com/embed/U4dF-UQ7k5E',
          images:[{
            id: 10,
            imgurl:'https://secure-media0.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/52/1000057152/PCTV-1000057152-vl.jpg'
          },
          {
            id: 10,
            imgurl: 'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/37/1770016137/PCTV-1770016137-vl.jpg'
           },
            {
              id: 10,
              imgurl:'https://secure-media1.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/33/1000082533/PCTV-1000082533-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/9394/49394-v'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/82/1000102582/PCTV-1000102582-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/17/1000064117/PCTV-1000064117-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/28/1000074328/PCTV-1000074328-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/07/1100001807/PCTV-1100001807-vl.jpg'
            }],
         },
         {
          id: 1,
          video: 'https://www.youtube.com/embed/3Ai8MRv9zwg',
          images:[{
            id: 10,
            imgurl:'https://secure-media0.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/52/1000057152/PCTV-1000057152-vl.jpg'
          },
          {
            id: 10,
            imgurl: 'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/37/1770016137/PCTV-1770016137-vl.jpg'
           },
            {
              id: 10,
              imgurl:'https://secure-media1.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/33/1000082533/PCTV-1000082533-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/9394/49394-v'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/82/1000102582/PCTV-1000102582-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/17/1000064117/PCTV-1000064117-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/28/1000074328/PCTV-1000074328-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/07/1100001807/PCTV-1100001807-vl.jpg'
            }],
        },
        {
          id: 2,
          video:'https://www.youtube.com/embed/ZkogUD3CtNw',
          images:[{
            id: 10,
            imgurl:'https://secure-media0.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/52/1000057152/PCTV-1000057152-vl.jpg'
          },
          {
            id: 10,
            imgurl: 'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/37/1770016137/PCTV-1770016137-vl.jpg'
           },
            {
              id: 10,
              imgurl:'https://secure-media1.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/33/1000082533/PCTV-1000082533-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/9394/49394-v'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/82/1000102582/PCTV-1000102582-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/17/1000064117/PCTV-1000064117-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/28/1000074328/PCTV-1000074328-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/07/1100001807/PCTV-1100001807-vl.jpg'
            }],
        },
        {
          id: 3,
          video:'https://www.youtube.com/embed/yOoW9pryvYo',
          images:[{
            id: 10,
            imgurl:'https://secure-media0.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/52/1000057152/PCTV-1000057152-vl.jpg'
          },
          {
            id: 10,
            imgurl: 'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/37/1770016137/PCTV-1770016137-vl.jpg'
           },
            {
              id: 10,
              imgurl:'https://secure-media1.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/33/1000082533/PCTV-1000082533-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/9394/49394-v'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/82/1000102582/PCTV-1000102582-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/17/1000064117/PCTV-1000064117-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/28/1000074328/PCTV-1000074328-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/07/1100001807/PCTV-1100001807-vl.jpg'
            }],
        },
        {
          id: 4,
          video: 'https://www.youtube.com/embed/E3aAkRpiErI',
          images:[{
            id: 10,
            imgurl:'https://secure-media0.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/52/1000057152/PCTV-1000057152-vl.jpg'
          },
          {
            id: 10,
            imgurl: 'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/37/1770016137/PCTV-1770016137-vl.jpg'
           },
            {
              id: 10,
              imgurl:'https://secure-media1.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/33/1000082533/PCTV-1000082533-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/9394/49394-v'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/82/1000102582/PCTV-1000102582-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/17/1000064117/PCTV-1000064117-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/28/1000074328/PCTV-1000074328-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/07/1100001807/PCTV-1100001807-vl.jpg'
            }],
        },
        {
          id: 5,
          video: 'https://www.youtube.com/embed/NJcRY9OelYc',
          images:[{
            id: 10,
            imgurl:'https://secure-media0.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/52/1000057152/PCTV-1000057152-vl.jpg'
          },
          {
            id: 10,
            imgurl: 'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/37/1770016137/PCTV-1770016137-vl.jpg'
           },
            {
              id: 10,
              imgurl:'https://secure-media1.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/33/1000082533/PCTV-1000082533-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/9394/49394-v'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/82/1000102582/PCTV-1000102582-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/17/1000064117/PCTV-1000064117-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/28/1000074328/PCTV-1000074328-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/07/1100001807/PCTV-1100001807-vl.jpg'
            }],
        },
          
    {
          id: 6,
          video:  'https://www.youtube.com/embed/q-Ltq9LZhPE',
          images:[{
            id: 10,
            imgurl:'https://secure-media0.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/52/1000057152/PCTV-1000057152-vl.jpg'
          },
          {
            id: 10,
            imgurl: 'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/37/1770016137/PCTV-1770016137-vl.jpg'
           },
            {
              id: 10,
              imgurl:'https://secure-media1.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/33/1000082533/PCTV-1000082533-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/9394/49394-v'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/82/1000102582/PCTV-1000102582-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/17/1000064117/PCTV-1000064117-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/28/1000074328/PCTV-1000074328-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/07/1100001807/PCTV-1100001807-vl.jpg'
            }],
        },
      {
          id: 7,
         video:  'https://www.youtube.com/embed/pdRVy5byVcA',
         images:[{
            id: 10,
            imgurl:'https://secure-media0.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/52/1000057152/PCTV-1000057152-vl.jpg'
          },
          {
            id: 10,
            imgurl: 'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/37/1770016137/PCTV-1770016137-vl.jpg'
           },
            {
              id: 10,
              imgurl:'https://secure-media1.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/33/1000082533/PCTV-1000082533-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/9394/49394-v'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/82/1000102582/PCTV-1000102582-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/17/1000064117/PCTV-1000064117-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/28/1000074328/PCTV-1000074328-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/07/1100001807/PCTV-1100001807-vl.jpg'
            }],
      },
        {
          id:'8',
          video: 'https://www.youtube.com/embed/U4dF-UQ7k5E',
          images:[{
            id: 10,
            imgurl:'https://secure-media0.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/52/1000057152/PCTV-1000057152-vl.jpg'
          },
          {
            id: 10,
            imgurl: 'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/37/1770016137/PCTV-1770016137-vl.jpg'
           },
            {
              id: 10,
              imgurl:'https://secure-media1.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/33/1000082533/PCTV-1000082533-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/9394/49394-v'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/82/1000102582/PCTV-1000102582-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_1x/r1/thumbs/PCTV/17/1000064117/PCTV-1000064117-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media2.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/28/1000074328/PCTV-1000074328-vl.jpg'
            },
            {
              id: 10,
              imgurl:'https://secure-media3.hotstar.com/t_web_vl_3x/r1/thumbs/PCTV/07/1100001807/PCTV-1100001807-vl.jpg'
            }],
        
      }],left:0,right:0,activeData:{}};
   }
   
   leftClick(){
    if(this.state.left!=-45)
    this.setState({left:this.state.left-15});
   
   }

 rightClick(){
    if(this.state.left!=0)
  this.setState({left:this.state.left+15});
   
}

   
componentDidMount(){
  
  let myData = this.state.MoviessListImgs.filter ((e1) =>{
    return e1.id == this.props.id
  })
  this.setState({activeData:myData[0]});

}
   render() {
    
  
      
      return (
         <div>
         <div className="mySlides slidesContainer" id="containerspan1" style={{left:this.state.left+"%"}}>
         <div>
            {/*<h1>{this.state.images.id}</h1>*/}
            { this.state.activeData.images!=undefined?
              <iframe className="video" width="35%"  src={this.state.activeData.video}/>:""
            }
            </div>
            {this.state.activeData.images!=undefined? this.state.activeData.images.map((e)=>{
            
              return <div className="mySlides" id="containerspan1">
              <img className="containericon"   src="data:image/svg+xml;base64,PHN2ZyBmaWxsPSIjZmZmZmZmIiBoZWlnaHQ9IjI0IiB2aWV3Qm94PSIwIDAgMjQgMjQiIHdpZHRoPSIyNCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgIDxwYXRoIGQ9Ik0wIDBoMjR2MjRIMHoiIGZpbGw9Im5vbmUiLz4KICAgIDxwYXRoIGQ9Ik0xNCAxMEgydjJoMTJ2LTJ6bTAtNEgydjJoMTJWNnptNCA4di00aC0ydjRoLTR2Mmg0djRoMnYtNGg0di0yaC00ek0yIDE2aDh2LTJIMnYyeiIvPgo8L3N2Zz4="alt="img"/>
 
              <Link to = {'/movies/'+e.id}>
            
            {/*  <iframe className="video" width="100%" height="195.25px" src={e.video}/>*/}
              < div className="containerimg">
             
               <img className="img1sub" width="147px" height="195.25px" src={e.imgurl }/>
               
              
             </div>
         {/*   <iframe className="video" width="147px" height="195.25px" src='video'/>*/}
         
         
              </Link>
               </div>
              
              
            }):""}
            </div>
            <button className="leftButtonsub" onClick={()=>{this.rightClick()}}>&#10094;</button>
            <button className="rightButtonsub" onClick={()=>{this.leftClick()}}>&#10095;</button>
         </div>
         
      );
     
        
      
   }
}

export default MoviessList;