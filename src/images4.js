import React from 'react';
import {Link} from 'react-router-dom';
class Images4 extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
        abc:[{
        id:0 ,
        imgurl: 'https://img3.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/2933/192933-h'
    },{
        id: 1,
        imgurl: 'https://img2.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/3332/193332-h'
      },{
        id: 2,
        imgurl: 'https://img2.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/3329/193329-h'
      },,{
        id: 3,
        imgurl: 'https://img0.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/3333/193333-h'
      },{
        id: 4,
        imgurl: 'https://img1.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/3315/193315-h'
      },{
        id: 5,
        imgurl: 'https://img2.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/3024/193024-h'
      },{
        id: 6,
        imgurl: 'https://img0.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/6748/156748-h'
      },{
        id: 7,
        imgurl: 'https://img2.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/2789/192789-h',
       },{
        id: 8,
        imgurl: 'https://img2.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/3024/193024-h',
       }],left:0,right:0};
   }
   leftClick(){
    if(this.state.left!=-45)
     this.setState({left:this.state.left-15});
    
    }
 
  rightClick(){
     if(this.state.left!=0)
   this.setState({left:this.state.left+15});
    
 }

   render() {
    
      return (
        <div>
        <div className="mySlides slidesContainer" id="containerspan1" style={{left:this.state.left+"%"}}>
           <h1>{this.state.id}</h1>
           {this.state.abc.map((e)=>{
             return <div className="mySlides myContainer" id="containerspan1news">
             <img className="containericonnews"  src="data:image/svg+xml;base64,PHN2ZyBmaWxsPSIjZmZmZmZmIiBoZWlnaHQ9IjI0IiB2aWV3Qm94PSIwIDAgMjQgMjQiIHdpZHRoPSIyNCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgIDxwYXRoIGQ9Ik0wIDBoMjR2MjRIMHoiIGZpbGw9Im5vbmUiLz4KICAgIDxwYXRoIGQ9Ik0xNCAxMEgydjJoMTJ2LTJ6bTAtNEgydjJoMTJWNnptNCA4di00aC0ydjRoLTR2Mmg0djRoMnYtNGg0di0yaC00ek0yIDE2aDh2LTJIMnYyeiIvPgo8L3N2Zz4="alt="img"/>

             <Link to = {'/sports/'+e.id}>
             
             <div className="containerimg"><img className="img1news" width="147px" height="195.25px" src={e.imgurl}/>
             
            
             </div>
             </Link>
            </div>
             
             
           })}
           </div>
           <button className="leftButtonnews" onClick={()=>{this.rightClick()}}>&#10094;</button>
           <button className="rightButtonnews" onClick={()=>{this.leftClick()}}>&#10095;</button>
        </div>
        
         
      );
     
        
      
   }
}

export default Images4;