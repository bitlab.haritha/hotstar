import React from 'react';
import {Link} from 'react-router-dom';
class SerialsList extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
        SerialsListImgs:[{
          id: 0,
          video: ' https://www.youtube.com/embed/Vdw0OPbM-34',
          images:[{
            id: 12,
            imgurl:'https://www.apherald.com/ImageStore/images/movies/movies-wallpapers/Agni-Sakshi-Serial-Actress-Aishwarya-Real-Life-Unseen-PHotos1.jpg'
          },
          {
            id: 13,
            imgurl: 'https://www.apherald.com/ImageStore/images/movies/movies-wallpapers/Agni-Sakshi-Serial-Actress-Aishwarya-Real-Life-Unseen-PHotos4.jpg'
           },
            {
              id: 14,
              imgurl:'https://www.apherald.com/ImageStore/images/movies/movies-wallpapers/Agni-Sakshi-Serial-Actress-Aishwarya-Real-Life-Unseen-PHotos3.jpg'
            },
            {
              id: 15,
              imgurl:'https://i.ytimg.com/vi/7fqt0hIC-zI/maxresdefault.jpg'
            },
            {
              id: 16,
              imgurl:'https://i.ytimg.com/vi/UiVSqInjEhg/maxresdefault.jpg'
            },
            {
              id: 17,
              imgurl:'https://i.ytimg.com/vi/OWNRPGsjaZ0/maxresdefault.jpg'
            },
            {
              id: 18,
              imgurl:'https://www.apherald.com/ImageStore/images/movies/movies-wallpapers/Agni-Sakshi-Serial-Actress-Aishwarya-Real-Life-Unseen-PHotos2.jpg'
            },
            {
              id: 19,
              imgurl:'https://www.apherald.com/ImageStore/images/movies/movies-wallpapers/Agni-Sakshi-Serial-Actress-Aishwarya-Real-Life-Unseen-PHotos18.jpg'
            }],

         },
         {
          id: 1,
          video: 'https://www.youtube.com/embed/SkODT2AjY2I',
          images:[{ id: 11,
            imgurl: 'http://crazum.com/wp-content/uploads/2018/09/Mounaragam-Heroine.jpg'},
            {id:12,
            imgurl:'http://crazum.com/wp-content/uploads/2018/09/Priyanka-Jain-as-Ammulu-in-Mounaraagam.jpg'},
            {id:13,
              imgurl:'http://crazum.com/wp-content/uploads/2018/09/Mounaragam-Daily-serial-telugu.jpg'},
              {id:14,
                imgurl:'http://crazum.com/wp-content/uploads/2018/09/Mounaraagam-Telugu-Serial-Maa-TV.jpg'},
                {id:15,
                  imgurl:'http://crazum.com/wp-content/uploads/2018/09/Priyanaka-M-Jain-Mounaraagam.jpg'},
                  {id:16,
                    imgurl:'http://crazum.com/wp-content/uploads/2018/09/Mounaraagam-Telugu-Serial-Heroine-Pics.jpg'},
                    {id:17,
                      imgurl:'http://crazum.com/wp-content/uploads/2018/09/Mounaraagam-Ammulu.jpg'},
                      {id:18,
                        imgurl:'http://crazum.com/wp-content/uploads/2018/09/Mounaraagam-Serial-Actress.jpg'},
                        {id:18,
                          imgurl:'http://crazum.com/wp-content/uploads/2018/09/Mounaraagam-Serial-Actress.jpg'}],
        },
        {
          id: 2,
          video: 'https://www.youtube.com/embed/aA7RhNpcdfo',
          images:[{ id: 12,
            imgurl: 'https://www.telugunestam.com/gallery/general/561704Jyothi-Telugu-Serial-in-star-maa.jpg'},
            {id:13,
            imgurl:'http://www.allindianblog.in/wp-content/uploads/2016/11/Maa-TV-Lakshmi-Kalyanam-Serial-Cast-Wiki-Actress-Actors.jpg'},
            {id:14,
              imgurl:'https://secure-media2.hotstar.com/r1/thumbs/PCTV/08/1000203808/PCTV-1000203808-hcdl.jpg'},
              {id:15,
                imgurl:'https://upload.wikimedia.org/wikipedia/en/3/37/Star_Maa.jng.jpg'},
                {id:16,
                  imgurl:'http://garagemeter.com/wp-content/uploads/2018/09/mou.png'},
                  {id:17,
                    imgurl:'https://secure-media.hotstar.com/r1/thumbs/ANDROID/74/1100006474/ANDROID-1100006474-hm_tab_xxhdpi.jpg'},
                    {id:18,
                      imgurl:'https://www.starepisodes.net/wp-content/uploads/2018/10/Shambhavi-11th-October-2018-Full-Episode-116-Watch-Online1.jpg'},
                      {id:19,
                        imgurl:'https://i.ytimg.com/vi/9QjRIBd7nRo/maxresdefault.jpg'}],
        },
        {
          id: 3,
          video: 'https://www.youtube.com/embed/XO0iYcw35no',
          images:[{ id: 12,
            imgurl: 'https://www.thehindu.com/life-and-style/article23759921.ece/ALTERNATES/FREE_460/hym27ashika1 '},
            {id:13,
            imgurl:'https://www.telugunestam.com/gallery/general/32429722Kathalo-Rajakumari-serial-in-Star-Maa.jpg'},
            {id:14,
              imgurl:'https://topindianshows.in/wp-content/uploads/2018/01/Kathalo-Rajakumari-Serial-o.jpg'},
              {id:15,
                imgurl:'https://i.ytimg.com/vi/D-2VIyTKuvg/hqdefault.jpg'},
                {id:16,
                  imgurl:'https://i.ytimg.com/vi/uGZRzA_ZwJU/hqdefault.jpg'},
                  {id:17,
                    imgurl:'http://i.ytimg.com/vi/vjUqP4Vv5-Q/mqdefault.jpg'},
                    {id:18,
                      imgurl:'https://i.ytimg.com/vi/muI_27si9Mk/hqdefault.jpg'},
                      {id:19,
                        imgurl:'https://i.ytimg.com/vi/6A1p-GqKL-Y/maxresdefault.jpg'}],
        },
        {
          id: 4,
          video: 'https://www.youtube.com/embed/-jLwHhvci3c',
          images:[{ id:12,imgurl:'https://secure-media2.hotstar.com/t_web_vl_2x/r1/thumbs/PCTV/01/9901/PCTV-9901-vl.jpg'},
          {id:13,
             imgurl:'https://secure-media.hotstar.com/r1/thumbs/ANDROID/95/1000179395/ANDROID-1000179395-hm_tab_xxhdpi.jpg'},
          { id:14,imgurl:'https://i.ytimg.com/vi/oEbQIbw0_m8/maxresdefault.jpg'},
          { id:15,imgurl:'https://secure-media.hotstar.com/r1/thumbs/ANDROID/65/1000188065/ANDROID-1000188065-hm_tab_xxhdpi.jpg'},
          { id:16,imgurl:'https://i.ytimg.com/vi/CDpfFGJBuoc/maxresdefault.jpg'},
          {id:17, imgurl:'https://i.ytimg.com/vi/BM6Y4Jjrzn8/hqdefault.jpg'},
         { id:18,imgurl:'https://secure-media.hotstar.com/r1/thumbs/ANDROID/51/1000109251/ANDROID-1000109251-hm_tab_xxhdpi.jpg'},
         {id:19, imgurl:'https://i.ytimg.com/vi/CQjck1t8W2c/maxresdefault.jpg'}],
        },
        {
          id: 5,
          video: 'https://www.youtube.com/embed/s3Dcuitr0n0',
          images:[{ id:12,imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_vl_2x/sources/r1/cms/prod/9305/89305-v'},
          {id:13,
             imgurl:'https://i.pinimg.com/originals/f7/4b/e8/f74be80bb5e2661268b6d18c0baeeba7.jpg'},
          { id:14,imgurl:'https://i.ytimg.com/vi/Efjz_4ifo5A/maxresdefault.jpg'},
          { id:15,imgurl:'https://i.ytimg.com/vi/LgAT92UoK3Y/maxresdefault.jpg'},
          { id:16,imgurl:'https://secure-media2.hotstar.com/r1/thumbs/PCTV/52/10752/PCTV-10752-hcdl.jpg'},
          {id:17, imgurl:'https://i.ytimg.com/vi/8CePf3n3iF8/maxresdefault.jpg'},
         { id:18,imgurl:'https://secure-media.hotstar.com/r1/thumbs/ANDROID/20/1100007920/ANDROID-1100007920-hm_tab_xxhdpi.jpg'},
         {id:19, imgurl:'https://static.toiimg.com/photo/msid-66633262/66633262.jpg?116855'}],
        },
          
    {
          id: 6,
          video: 'https://www.youtube.com/embed/fAWRMnC8UWA',
          images:[{id:12, imgurl:'https://img1.hotstar.com/image/upload/f_auto,t_web_vl_2x/sources/r1/cms/prod/9278/119278-v'},
          {id:13, imgurl:'https://timesofindia.indiatimes.com/thumb/msid-66567610,imgsize-88308,width-400,resizemode-4/66567610.jpg'},
          {id:14, imgurl:'https://c.ndtvimg.com/ilv67f5g_monalisa-nazar_625x300_31_July_18.jpg'},
          { id:15,imgurl:'https://www.onenov.in/wp-content/uploads/2018/10/4.jpg'},
         {id:16, imgurl:'https://www.onenov.in/wp-content/uploads/2018/10/10.jpg'},
          {id:17, imgurl:'https://s3-ap-southeast-1.amazonaws.com/engpeepingmoon/wp-content/uploads/2018/09/Monalisa-turns-choreographer-for-her-TV-show-Nazar.jpg'},
         {id:18, imgurl:'https://i.ytimg.com/vi/NbatC8mHCvQ/maxresdefault.jpg'},
          {id:19, imgurl:'https://s1.dmcdn.net/tpMt0/x360-pYj.jpg'}],
        },
      {
          id: 7,
         video:  'https://www.youtube.com/embed/NDPOvSXAdqQ',
          images:[{ id:12,imgurl:'https://secure-media2.hotstar.com/t_web_vl_2x/r1/thumbs/PCTV/45/17745/PCTV-17745-vl.jpg'},
          {id:13, imgurl:'https://i.ytimg.com/vi/c1asXQMoEnQ/maxresdefault.jpg'},
         {id:14, imgurl:'http://www.manatelugu.to/wp-content/uploads/2018/05/kante-265x198.jpg'},
         { id:15,imgurl:'https://topindianshows.in/wp-content/uploads/2018/04/Kante-Koothurne-Kanali-Seri-730x430.jpg'},
         {id:16,imgurl:'https://secure-media.hotstar.com/r1/thumbs/ANDROID/58/1100005258/ANDROID-1100005258-hm_tab_xxhdpi.jpg'},
          {id:17, imgurl:'https://img2.hotstar.com/image/upload/f_auto,t_hcdl/sources/r1/cms/prod/109/50109-h'},
         {id:18, imgurl:'https://timesofindia.indiatimes.com/thumb/msid-63731605,width-400,resizemode-4/63731605.jpg'},
          {id:19, imgurl:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPJOSF9ke0TleWxi_N5lBsDLQJESPb7-yLUJeRriWXYJGWdGZ-'
        }],
      },
        {
          id:'8',
          video: 'https://www.youtube.com/embed/Me2S3Fd0jTQ',
          images:[{id:12, imgurl:'https://secure-media2.hotstar.com/r1/thumbs/PCTV/57/15457/PCTV-15457-vl.jpg'},
          {id:13, imgurl:'https://i.ytimg.com/vi/QQEOsSrAC0E/hqdefault.jpg'},
          {id:14, imgurl:'https://timesofindia.indiatimes.com/thumb/msid-65852687,imgsize-53234,width-400,resizemode-4/65852687.jpg'},
          {id:15, imgurl:'https://timesofindia.indiatimes.com/thumb/msid-65806224,imgsize-162630,width-400,resizemode-4/65806224.jpg'},
          {id:16, imgurl:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRJ6QmLDtdIOtB8gHjXuhma6ObHQx7LUzHyO37N9AcYmEUV8TzOOg'},
         {id:17, imgurl:'https://img.youtube.com/vi/i7JpFEd8-e0/mqdefault.jpg'},
          { id:18,imgurl:'https://i.ytimg.com/vi/5FYam7iGwsA/maxresdefault.jpg'},
          {id:19, imgurl:'https://i.ytimg.com/vi/0V3Clz5-nJ4/mqdefault.jpg'}],
        
      }],left:0,right:0,activeData:{}};
   }
   
   leftClick(){
    if(this.state.left!=-45)
    this.setState({left:this.state.left-15});
   
   }

 rightClick(){
    if(this.state.left!=0)
  this.setState({left:this.state.left+15});
   
}
   
componentDidMount(){
  
  let myData = this.state.SerialsListImgs.filter ((e1) =>{
    return e1.id == this.props.id
  })
 
  this.setState({activeData:myData[0]});
}
   render() {
    
  
      
      return (
         <div >
         <div className="mySlides slidesContainer" id="containerspan1" style={{left:this.state.left+"%"}}>
            <div>{ this.state.activeData.images!=undefined?
              <iframe className="video" width="35%"  src={this.state.activeData.video}/>:""
            }
            </div>
            {this.state.activeData.images!=undefined? this.state.activeData.images.map((e)=>{
            
              return <div className="mySlides" id="containerspan1">
             
              <Link to = {'/page/'+e.id}>
              < div className="containerimg">
             
               <img className="img1sub" width="147px" height="195.25px" src={e.imgurl }/>
             </div>
        
          
         
              </Link>
               </div>
              
              
            }):""}
            </div>
            <button className="leftButtonsub" onClick={()=>{this.rightClick()}}>&#10094;</button>
            <button className="rightButtonsub" onClick={()=>{this.leftClick()}}>&#10095;</button>
         </div>
         
      );
     
        
      
   }
  //  componentWillMount(){
  
  //   let myData = this.state.SerialsListImgs.map ((e1) =>{
  //     return e1.id.video == this.props.id.video
  //   })
   
  //   this.setState({activeData:myData[0]});
  // }
  
}

export default SerialsList;