import React from 'react';
import {Link} from 'react-router-dom';
class LiveNews extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
        LiveNewsImgs:[{
          id: 0,
          video:'https://www.youtube.com/embed/v-bZONX2rp4 ',
          images:[{
            id: 12,
            imgurl:'https://c1.staticflickr.com/5/4570/24804684898_7c8567c41a_z.jpg'
          },
          {
            id: 13,
            imgurl: 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201704/chahattisgarh-news-reader-news-husband-death-accident-647_040817112047.jpg'
           },
            {
              id: 14,
              imgurl:'https://yt3.ggpht.com/a-/AAuE7mBKVWQl9E-729CUmKBOVbTWxsc13lR_HrzNAw=s900-mo-c-c0xffffffff-rj-k-no'
            },
            {
              id: 15,
              imgurl:'https://i.ytimg.com/vi/_VXD8ZuATCU/hqdefault.jpg'
            },
            {
              id: 16,
              imgurl:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTKcIR1GrUK8ZwxRhACe6CRbaiurJugjoyV-Jh_V1COJxuTJKXH'
            },
            {
              id: 17,
              imgurl:'https://secure-media.hotstar.com/r1/thumbs/ANDROID/49/1000119249/ANDROID-1000119249-hm_tab_xxhdpi.jpg'
            },
            {
              id: 18,
              imgurl:'https://i.ytimg.com/vi/spmwEjef2DE/hqdefault.jpg'
            },
            {
              id: 19,
              imgurl:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6_gVwEn3SV6wwfnMiuZbDQrvQiujv-5pwOu9_KeDcsAnpDM0n'
            }],

         },
         {
          id: 1,
          video: ' https://www.youtube.com/embed/8mplN0_Eieg',
          images:[{ id: 11,
            imgurl: 'http://www.bestmediainfo.com/wp-content/uploads/2016/08/btvi-logo.jpg'},
            {id:12,
            imgurl:'http://www.newsexperts.in/wp-content/uploads/2018/09/btvi.jpg'},
            {id:13,
              imgurl:'https://www.btvi.in/media/images/videos_thumbs/large/empspknivrita.jpg'},
              {id:14,
                imgurl:'https://www.btvi.in/media/images/videos_thumbs/large/fd88ecf901f7b1474ba10b0bec7eb044.jpg'},
                {id:15,
                  imgurl:'https://i.ytimg.com/vi/BPIfLygC4X0/hqdefault.jpg'},
                  {id:16,
                    imgurl:'https://www.btvi.in/media/images/videos_thumbs/large/791b3a4863054158be649733aec8e79b.jpg'},
                    {id:17,
                      imgurl:'https://www.btvi.in/media/images/videos_thumbs/large/47a49134ae1f9f238e7d16ca3fd41e11.jpg'},
                      {id:18,
                        imgurl:'https://www.btvi.in/media/images/videos_thumbs/large/c7519e37dd2332c1ce0b828c2698682e.jpg'},
                        {id:18,
                          imgurl:'https://image-store.slidesharecdn.com/dde19d52-26c7-479e-91c0-018119f72116-large.jpeg'}],
        },
        {
          id: 2,
          video: 'https://www.youtube.com/embed/0J4g6hNtNxM',
          images:[{ id: 12,
            imgurl: 'https://img0.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/2861/192861-h'},
            {id:14,
              imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/2861/192861-h'},
              {id:15,
                imgurl:'https://static.asianetnews.com/images/01cntkv7zn1hs7mt0002664cvv/vlcsnap_2018_08_26_13h26m19s990_opt_710x400xt.png'},
                {id:16,
                  imgurl:'https://freepresskashmir.com/wp-content/uploads/2018/05/Aditya-raj-kaul.jpg'},
                  {id:17,
                    imgurl:'https://images.indianexpress.com/2017/05/re-759.jpg'},
                    {id:18,
                      imgurl:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDZDJ08tDhRKLEiXvdXQ3wvMOKDR1ER-uJrUCZ-Gl_S6H6_G51Zg'},
                      {id:19,
                        imgurl:'https://i.ytimg.com/vi/YPu3ZEHlFPc/hqdefault.jpg'}],
        },
        {
          id: 3,
          video: 'https://www.youtube.com/embed/WzY_-tx3M3c',
          images:[{ id: 12,
            imgurl: 'https://img0.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/2861/192861-h'},
            {id:13,
            imgurl:'https://i.ytimg.com/vi/AkNuSUq8nTk/maxresdefault.jpg'},
            {id:14,
              imgurl:'https://i.ytimg.com/vi/ct-Ul-9jX-E/maxresdefault.jpg'},
              {id:15,
                imgurl:'https://i.ytimg.com/vi/OouP0TIfO6c/maxresdefault.jpg'},
                {id:16,
                  imgurl:'https://i.ytimg.com/vi/sdcxjHmNyVM/hqdefault.jpg'},
                  {id:17,
                    imgurl:'https://i.ytimg.com/vi/sHmSHfiVIrU/hqdefault.jpg'},
                    {id:18,
                      imgurl:'https://i.ytimg.com/vi/Ev1VXjK__Jg/hqdefault.jpg'},
                      {id:19,
                        imgurl:'https://images.adgully.com/img/800/66325_bloombergquint.jpg'}],
        },
        {
          id: 4,
         video:  'https://www.youtube.com/embed/nxyWdf1OsB4',
          images:[{ id:12,imgurl:'https://smedia2.intoday.in/btmt/images/stories/indiatoday-660_020218043532.jpg'},
          {id:13, imgurl:'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201506/india-today-tv--1-647_061315043545.jpg'},
         {id:14, imgurl:'https://d2na0fb6srbte6.cloudfront.net/read/imageapi/coverforissue/677312/magazine/300/new'},
         { id:15,imgurl:'http://www.bestmediainfo.com/wp-content/uploads/2017/01/ITE-Cover-Feb6.jpg'},
         {id:16,imgurl:'https://resize.indiatvnews.com/en/resize/newbucket/715_-/2016/10/prabhas-1475371077.jpg'},
          {id:17, imgurl:'https://www.trendrr.net/wp-content/uploads/2017/04/India-Today.jpg'},
         {id:18, imgurl:'http://3.bp.blogspot.com/_eAJS2Lr3MUg/SINwxUhMgXI/AAAAAAAAAA0/fJ3GcsZOMZk/s320/Tantra.jpg'},
          {id:19, imgurl:'https://www.trendrr.net/wp-content/uploads/2017/04/BBC-News-Top-10-Best-News-Channels-in-The-World-2017.jpg'
        }],
      },
      {
        id: 5,
        video: 'https://www.youtube.com/embed/13utStSqRNM',
        images:[{ id:12,imgurl:'https://images.mathrubhumi.com/images/Mathrubhumi_logo.png'},
        {id:13,
           imgurl:'http://www.indiantelevision.com/sites/default/files/styles/smartcrop_800x800/public/images/tv-images/2018/07/26/Mathrubhumi.jpg?itok=AvdliHVF'},
        { id:14,imgurl:'https://4.bp.blogspot.com/-0Z3_epeRYXw/WXX-5F7MUfI/AAAAAAAABiY/ssdW7qkjv5cBEZArmxppLWQrnazSLRywwCLcBGAs/s1600/Smruthy%2B20130318%2Bmon%2B1801.jpg'},
        { id:15,imgurl:'https://2.bp.blogspot.com/-nZ6BzlW8iF4/WXX-6Xu426I/AAAAAAAABik/Hu5E-UB_EDsdhLYsZby51pR14C8ExHalQCLcBGAs/s1600/Smruthy%2B20130618%2Btue%2B1515.jpg'},
        { id:16,imgurl:'https://i.ytimg.com/vi/WN0FJeAJ3tY/hqdefault.jpg'},
        {id:17, imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/2861/192861-h'},
       { id:18,imgurl:'https://i.ytimg.com/vi/CZ_qf-SxDmM/hqdefault.jpg'},
       {id:19, imgurl:'https://www.thenewsminute.com/sites/default/files/styles/news_detail/public/Venu_Balakrishnan_mathrubhumi_news_750.jpg?itok=ar4HyQWM'}],
      },
          
      {
        id: 6,
        video: 'https://www.youtube.com/embed/XOacA3RYrXk',
        images:[{ id:12,imgurl:'https://lh3.googleusercontent.com/oGA33jKoL-05q8wTm3AAuYryOyZhn2oJbb9K38ULmbGsdtAsU8TxPG6_s9aMZkgpAzI'},
        {id:13,
           imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/2861/192861-h'},
        { id:14,imgurl:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPA_ogqqixccHUUG6TMPBBw1xEMSIBiVkJxj3ONMRBa5maDLZy'},
        { id:15,imgurl:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYvXE7z44xq5N8OVpr_d_W8b9B9kTznjuQY6ZYLcjK39eTm2K4'},
        { id:16,imgurl:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTe5U-_M8oEMCgXYqIECbjLho3GM_N1oILZ6jPFeJNpP5h0TPhX'},
        {id:17, imgurl:'https://static.standard.co.uk/s3fs-public/thumbnails/image/2016/09/11/12/emily-thornberry.jpg'},
       { id:18,imgurl:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTyJfpWhut0Ka0zjbK0z82Fa-nUJ2tpk5MwC1ld8HBTvyYOpXEu'},
       {id:19, imgurl:'https://httpsak-a.akamaihd.net/2540076170001/2540076170001_5602568997001_5602537791001-vs.jpg?pubId=2540076170001&videoId=5602537791001'}],
      },
        
      {
        id: 7,
        video: 'https://www.youtube.com/embed/VQ60XbCJ7_g',
        images:[{id:12, imgurl:'https://static.foxnews.com/static/orion/styles/img/fox-news/og/og-fox-news.png'},
        {id:13, imgurl:'https://oilcitywyo.com/wp-content/uploads/2018/12/120718-HeatherNauert.jpg'},
        {id:14, imgurl:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQHFzxE88lChjJn1z6RkcIl5RkPC1Ieb_uRLQRJ09WPJFvgVQ4N'},
        { id:15,imgurl:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIvpMu3hci3r30ZCoSVQCoad_wyel8TwsGo5SKQ8I-L7JYi_qE'},
       {id:16, imgurl:'https://s3.amazonaws.com/abn-prod/wp-content/uploads/sites/3/2017/04/KellyW.jpg'},
        {id:17, imgurl:'https://i.ytimg.com/vi/kUZjhZdD1kw/maxresdefault.jpg'},
       {id:18, imgurl:'http://3.bp.blogspot.com/-xCAZ0iNTCP4/VWPtDSoyqgI/AAAAAAAA4K8/YTEZ74wT0Ec/s1600/Fox0a.jpg'},
        {id:19, imgurl:'https://img0.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/2861/192861-h'}],
      },
    
      {
        id:'8',
        video: 'https://www.youtube.com/embed/Me2S3Fd0jTQ',
        images:[{ id: 12,
          imgurl: 'https://img0.hotstar.com/image/upload/f_auto,t_web_hs_3x/sources/r1/cms/prod/2861/192861-h'},
          {id:13,
          imgurl:'https://i.ytimg.com/vi/AkNuSUq8nTk/maxresdefault.jpg'},
          {id:14,
            imgurl:'https://i.ytimg.com/vi/ct-Ul-9jX-E/maxresdefault.jpg'},
            {id:15,
              imgurl:'https://i.ytimg.com/vi/OouP0TIfO6c/maxresdefault.jpg'},
              {id:16,
                imgurl:'https://i.ytimg.com/vi/sdcxjHmNyVM/hqdefault.jpg'},
                {id:17,
                  imgurl:'https://i.ytimg.com/vi/sHmSHfiVIrU/hqdefault.jpg'},
                  {id:18,
                    imgurl:'https://i.ytimg.com/vi/Ev1VXjK__Jg/hqdefault.jpg'},
                    {id:19,
                      imgurl:'https://images.adgully.com/img/800/66325_bloombergquint.jpg'}],
      
    }],left:0,right:0,activeData:{}};
 }
   leftClick(){
    if(this.state.left!=-45)
    this.setState({left:this.state.left-15});
   
   }

 rightClick(){
    if(this.state.left!=0)
  this.setState({left:this.state.left+15});
   
}

   
componentDidMount(){
  
  let myData = this.state.LiveNewsImgs.filter ((e1) =>{
    return e1.id == this.props.id
  })
  this.setState({activeData:myData[0]});

}
   render() {
    
  
      
      return (
         <div>
         <div className="mySlides slidesContainer" id="containerspan1" style={{left:this.state.left+"%"}}>
         <div>
            {/*<h1>{this.state.images.id}</h1>*/}
            { this.state.activeData.images!=undefined?
              <iframe className="video" width="35%"  src={this.state.activeData.video}/>:""
            }
            </div>
            {this.state.activeData.images!=undefined? this.state.activeData.images.map((e)=>{
            
              return <div className="mySlides" id="containerspan1">
             
              <Link to = {'/movies/'+e.id}>
            
            {/*  <iframe className="video" width="100%" height="195.25px" src={e.video}/>*/}
              < div className="containerimg">
             
               <img className="img1sub" width="147px" height="195.25px" src={e.imgurl }/>
               
              
             </div>
         {/*   <iframe className="video" width="147px" height="195.25px" src='video'/>*/}
         
         
              </Link>
               </div>
              
              
            }):""}
            </div>
            <button className="leftButtonsub" onClick={()=>{this.rightClick()}}>&#10094;</button>
            <button className="rightButtonsub" onClick={()=>{this.leftClick()}}>&#10095;</button>
         </div>
         
      );
     
        
      
   }
}

export default  LiveNews;