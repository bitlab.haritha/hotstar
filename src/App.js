import React, { Component } from 'react';
// import logo from './logo.svg';
import {Link,Switch,Route} from 'react-router-dom';
// import Images from './images';
// import Images1 from './images1';
// import Images2 from './images2';
import Header from './Header';
import Subpage from './Subpage';
import Subpage1 from './Subpage1';
import Subpage2 from './Subpage2';
import Subpage3 from './Subpage3';

import './App.css';
import './titledivs.css';
import './loginform.css';
import './search.css';
import './premium.css';
import './footer.css';

class App extends Component{
  render() {
    return (
 
        <div>
        <Switch>
        <Route exact path="/" component={Header} />
           <Route exact path="/page/:id" component={Subpage} />
           <Route exact path="/movies/:id" component={Subpage1} />
           <Route exact path="/news/:id" component={Subpage2} />
           <Route exact path="/sports/:id" component={Subpage3} />
             </Switch>
             </div>
 
    );
  }
}
export default App;